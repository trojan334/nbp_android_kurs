package com.example.natalka.nbp;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.example.natalka.nbp.models.Table;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Maciej Czapla on 15.12.2018
 */
public class CurrencyActivity extends ArrowActivity {

    private ProgressBar progressBar;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_currency);

        // Pobranie danych
        fetchData();
        // Ustawienia toolbara (górnej belki aplikacji)
        setToolbar();

        progressBar = findViewById(R.id.progress_bar);
    }

    private void setToolbar() {
        if (getSupportActionBar() != null) {
            // Ustawienie tytułu
            getSupportActionBar().setTitle("KURSY WALUT");
        }
    }

    private void fetchData() {
        Call<List<Table>> call = NbpApi.getApi().getCurrencies();
        call.enqueue(getCallback());
    }

    private Callback<List<Table>> getCallback() {
        return new Callback<List<Table>>() {
            @Override
            public void onResponse(Call<List<Table>> call, Response<List<Table>> response) {
                progressBar.setVisibility(View.GONE);

                // Sprawdzenie czy kod odpowiedzi jest poprawny
                // tzn. w zakresie 200 - 299
                if (response.isSuccessful()
                        && response.body() != null
                        && !response.body().isEmpty()) {

                    // Wyżwietlenie danych
                    showData(response.body().get(0));

                }
            }

            @Override
            public void onFailure(Call<List<Table>> call, Throwable t) {
                progressBar.setVisibility(View.GONE);

                t.printStackTrace();
                String message = t.getMessage();

                // Wypełnienie i wyświetlenie błędu
                TextView error = findViewById(R.id.error);
                error.setText(message);
                error.setVisibility(View.VISIBLE);
            }
        };
    }

    private void showData(Table table) {
        RecyclerView recyclerView = findViewById(R.id.recycler_view);

        CurrencyAdapter adapter = new CurrencyAdapter(table.getRates());
        recyclerView.setAdapter(adapter);

        recyclerView.setLayoutManager(new LinearLayoutManager(this));

//        DividerItemDecoration divider =
//                new DividerItemDecoration(this, LinearLayoutManager.VERTICAL);
//
//        recyclerView.addItemDecoration(divider);
    }


}
