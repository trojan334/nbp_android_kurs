package com.example.natalka.nbp.models;

import com.google.gson.annotations.SerializedName;

public class Rate {

    @SerializedName("currency")
    private String name;

    @SerializedName("code")
    private String code;

    @SerializedName("mid")
    private float mid;


    public String getName() {
        return name;
    }

    public String getCode() {
        return code;
    }

    public float getMid() {
        return mid;
    }
}
