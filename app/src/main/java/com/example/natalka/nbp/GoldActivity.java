package com.example.natalka.nbp;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Spinner;

import com.example.natalka.nbp.models.Gold;
import com.github.mikephil.charting.charts.LineChart;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.LineData;
import com.github.mikephil.charting.data.LineDataSet;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Maciej Czapla on 15.12.2018
 */
public class GoldActivity extends ArrowActivity {

    private List<Integer> days = Arrays.asList(
            10, 20, 30, 50, 80, 90
    );

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_gold);

        setSpinner();
    }

    private void setSpinner() {
        Spinner spinner = findViewById(R.id.spinner);

        ArrayAdapter<Integer> adapter =
                new ArrayAdapter<>(
                        this,
                        android.R.layout.simple_spinner_item,
                        days
                );

        spinner.setAdapter(adapter);

        // Dodanie reakcji na wybór elementu ze spinnera
        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                fetchData(days.get(position));
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }

    private void fetchData(Integer numberOfElements) {
        Call<List<Gold>> call = NbpApi.getApi().getGold(numberOfElements);
        call.enqueue(getCallback());
    }

    private Callback<List<Gold>> getCallback() {
        return new Callback<List<Gold>>() {
            @Override
            public void onResponse(Call<List<Gold>> call, Response<List<Gold>> response) {
                if (response.body() != null && !response.body().isEmpty()) {
                    showData(response.body());
                }
            }

            @Override
            public void onFailure(Call<List<Gold>> call, Throwable t) {
                t.printStackTrace();
            }
        };
    }

    private void showData(List<Gold> goldRates) {
        LineChart chart = findViewById(R.id.chart);

        chart.clear();

        ArrayList<Entry> values = new ArrayList<>();

        for (int position = 0; position < goldRates.size(); position++) {
            Entry entry = new Entry(position,
                    goldRates.get(position).getPrice());

            values.add(entry);
        }

        LineDataSet set = new LineDataSet(values, "Kursy złota");
        set.setMode(LineDataSet.Mode.CUBIC_BEZIER);

        set.setColor(ContextCompat.getColor(this, R.color.colorPrimaryDark));

        set.setFillColor(getResources().getColor(R.color.colorPrimary));

        set.setDrawFilled(true);

        set.setDrawCircles(false);
        set.setLineWidth(8f);


        LineData data = new LineData(set);
        data.setValueTextSize(10f);

        chart.setData(data);

        chart.invalidate();
    }

}
