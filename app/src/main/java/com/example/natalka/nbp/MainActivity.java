package com.example.natalka.nbp;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // Ustawienie widoku aplikacji (z folderu layouts)
        setContentView(R.layout.activity_main);

        setupUi();
    }

    private void setupUi() {
        Button first = findViewById(R.id.first_button);
        Button second = findViewById(R.id.second_button);

        // Dodanie listenera reagującego na "krotkie" kliknięcie
        first.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this,
                        GoldActivity.class);

                // Otwarcie nowej aktywności
                startActivity(intent);
            }
        });

        second.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this,
                        CurrencyActivity.class);

                startActivity(intent);
            }
        });

    }
}
