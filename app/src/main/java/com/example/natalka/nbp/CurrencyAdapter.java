package com.example.natalka.nbp;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.natalka.nbp.models.Rate;

import java.util.List;


/**
 * Created by Maciej Czapla on 15.12.2018
 */
public class CurrencyAdapter extends RecyclerView.Adapter<CurrencyAdapter.Row> {

    private List<Rate> rates;

    CurrencyAdapter(List<Rate> rates) {
        this.rates = rates;
    }

    @NonNull
    @Override
    public Row onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater
                .from(viewGroup.getContext())
                .inflate(R.layout.currency_row, viewGroup, false);

        return new Row(view);
    }

    @Override
    public void onBindViewHolder(@NonNull Row row, int i) {
        Rate rate = rates.get(i);

        row.bind(rate);
    }

    @Override
    public int getItemCount() {
        return rates.size();
    }

    class Row extends RecyclerView.ViewHolder {
        // Elementy składowe wiersza
        private TextView name;
        private TextView description;
        private TextView mid;

        Row(@NonNull View itemView) {
            super(itemView);

            name = itemView.findViewById(R.id.name);
            description = itemView.findViewById(R.id.description);
            mid = itemView.findViewById(R.id.mid);
        }

        void bind(Rate rate) {
            name.setText(rate.getCode());
            description.setText(rate.getName());
            mid.setText(String.format("%f", rate.getMid()));
        }
    }

}
