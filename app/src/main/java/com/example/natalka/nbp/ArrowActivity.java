package com.example.natalka.nbp;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;

/**
 * Created by Maciej Czapla on 16.12.2018
 */
abstract class ArrowActivity extends AppCompatActivity {

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setToolbar();
    }

    private void setToolbar() {
        if (getSupportActionBar() != null) {
            // Dodanie obsługi "strzałki" wstecz
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
        }
    }

    @Override
    final public boolean onOptionsItemSelected(MenuItem item) {
        // Obsługa kliknięcia strzałki w górnym menu
        if (item.getItemId() == android.R.id.home) {
            // Zamknięcie obecnej aktywności
            finish();
        }

        return super.onOptionsItemSelected(item);
    }

}
