package com.example.natalka.nbp;

import com.example.natalka.nbp.models.Gold;
import com.example.natalka.nbp.models.Table;

import java.util.List;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Call;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.http.GET;
import retrofit2.http.Path;

/**
 * Created by Maciej Czapla on 15.12.2018
 */
public interface NbpApi {

    String BASE_URL = "http://api.nbp.pl/api/";

    @GET("exchangerates/tables/A")
    Call<List<Table>> getCurrencies();

    @GET("cenyzlota/last/{number}")
    Call<List<Gold>> getGold(@Path("number") int numberOfElements);


    static NbpApi getApi() {
        return NbpApi.getRetrofit().create(NbpApi.class);
    }

    static Retrofit getRetrofit() {
        // Dodanie logów do pobierania danych
        HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
        interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);

        OkHttpClient client = new OkHttpClient.Builder()
                .addInterceptor(interceptor)
                .build();

        return new Retrofit.Builder()
                .baseUrl(BASE_URL)
                .client(client)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
    }
}
